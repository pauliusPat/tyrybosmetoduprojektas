# for details on f(K) metric see
# http://www.ee.columbia.edu/~dpwe/papers/PhamDN05-kmeans.pdf
# https://datasciencelab.wordpress.com/2014/01/21/selection-of-k-in-k-means-clustering-reloaded/

compute_fk <- function(k, sse, prev_sse, dimension) {
  if (k == 1 || prev_sse == 0) {
    return(1)
  }

  weight_factor <- function(k, dimension) {
    weight_k2 <- 1 - 3 / (4 * dimension)
    weight_factor_accumulator(weight_k2, k)
  }

  weight_factor_accumulator <- function(acc, k) {
    if (k == 2) {
      return(acc)
    }
    weight_factor_accumulator(acc + (1 - acc) / 6, k - 1)
  }

  weight = weight_factor(k , dimension)
  sse / (weight * prev_sse)
}
